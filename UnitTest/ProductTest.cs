using GestaoProdutosAPI.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTest
{
    [TestClass]
    public class ProductTest
    {
        [TestMethod]
        public void TestManufacturingDate()
        {
            Product product = new Product{ ManufacturingDate = DateTime.Now, ExpirationDate = DateTime.Now.AddDays(-1)};
            var invalid = product.IsManufacturingDateInvalid();
            Assert.IsTrue(invalid);
            product.ExpirationDate = product.ManufacturingDate.Value;
            invalid = product.IsManufacturingDateInvalid();
            Assert.IsTrue(invalid);
            product.ExpirationDate = product.ExpirationDate.Value.AddDays(2);
            invalid = product.IsManufacturingDateInvalid();
            Assert.IsFalse(invalid);
        }
    }
}
