CREATE TABLE Products (
	Id INT NOT NULL IDENTITY PRIMARY KEY,
	Description VARCHAR(128) NOT NULL,
	Active BIT,
	ManufacturingDate DATE,
	ExpirationDate DATE,
	SupplierId INT,
	SupplierDescription VARCHAR(128),
	SupplierCNPJ VARCHAR(128)
);