﻿using System.ComponentModel.DataAnnotations;
using System;

namespace GestaoProdutosAPI.DB
{
    public class Product
    {
        [Dapper.Contrib.Extensions.Key]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int SupplierId { get; set; }
        public string SupplierDescription { get; set; }
        public string SupplierCNPJ { get; set; }
    }
}
