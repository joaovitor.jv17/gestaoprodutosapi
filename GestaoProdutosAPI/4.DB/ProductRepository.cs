﻿using AutoMapper;
using Dapper;
using Dapper.Contrib.Extensions;
using GestaoProdutosAPI.UseCases;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace GestaoProdutosAPI.DB
{
    public class ProductRepository : IProductRepository
    {
        private readonly IMapper _mapper;
        private readonly SqlConnection _connection;

        public ProductRepository(IMapper mapper, SqlConnection connection)
        {
            _mapper = mapper;
            _connection = connection;
        }
        public void Delete(Entities.Product product)
        {
            _connection.Update(product);
        }
        public void Edit(Entities.Product product)
        {
            _connection.Update(product);
        }
        public Entities.Product Get(int id)
        {
            var product = _connection.Get<Entities.Product>(id);
            return product;
        }
        public Entities.Product Insert(Entities.Product product)
        {
            var obj = _mapper.Map<DB.Product>(product);
            var id = _connection.Insert(obj);
            product.Id = (int)id;
            return product;
        }
        public IEnumerable<Entities.Product> List(ProductListRequest request)
        {
            string sql = @$" SELECT * {GetListSqlFrom(request)} ORDER BY Id OFFSET {(request.Paginator.Page - 1) * request.Paginator.RowsPerPage} ROWS FETCH NEXT {request.Paginator.RowsPerPage} ROWS ONLY";
            var products = _connection.Query<Entities.Product>(sql, request);
            return products;
        }
        public int ListCount(ProductListRequest request)
        {
            string sql = $"SELECT COUNT(*) {GetListSqlFrom(request)}";
            var count = _connection.ExecuteScalar<int>(sql, request);
            return count;
        }
        private string GetListSqlFrom(ProductListRequest request)
        {
            return @$" FROM Products WHERE 
Description Like {(string.IsNullOrEmpty(request.Description) ? "Description" : $"'%{request.Description}%'")} AND
Active = {(request.Active != null ? "@Active" : "Active")} AND
ManufacturingDate >= {(request.ManufacturingDateInitial != null ? "@ManufacturingDateInitial" : "ManufacturingDate")} AND
ManufacturingDate <= {(request.ManufacturingDateEnd != null ? "@ManufacturingDateEnd" : "ManufacturingDate")} AND
ExpirationDate >= {(request.ExpirationDateInitial != null ? "@ExpirationDateInitial" : "ExpirationDate")} AND
ExpirationDate <= {(request.ExpirationDateEnd != null ? "@ExpirationDateEnd" : "ExpirationDate")} AND
SupplierDescription Like {(string.IsNullOrEmpty(request.SupplierDescription) ? "SupplierDescription" : $"'%{request.SupplierDescription}%'")} ";
        }


        ~ProductRepository()
        {
            _connection.Close();
            _connection.Dispose();
        }
    }
}
