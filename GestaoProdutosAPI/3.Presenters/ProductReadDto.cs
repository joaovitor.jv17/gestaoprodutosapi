﻿using System;

namespace GestaoProdutosAPI.Presenters
{
    public class ProductReadDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public DateTime ManufacturingDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string SupplierDescription { get; set; }
    }
}
