﻿using AutoMapper;
using GestaoProdutosAPI.Entities;
using GestaoProdutosAPI.UseCases;

namespace GestaoProdutosAPI.Presenters
{
    public class DtoConfigurationMapping : Profile
    {
        public DtoConfigurationMapping()
        {
            CreateMap<Product, ProductReadDto>().ForMember(x => x.Status, y => y.MapFrom(p => p.Active ? "Active" : "Inactive"));
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductEditDto, Product>();
            CreateMap<ProductListResponse, ProductListDto>();
            CreateMap<Entities.Product, DB.Product>();

        }

    }
}
