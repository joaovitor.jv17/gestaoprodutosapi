﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GestaoProdutosAPI.Presenters
{
    public class ProductEditDto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        private bool Active { get; set; }
        public DateTime ManufacturingDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int SupplierId { get; set; }
        public string SupplierDescription { get; set; }
        public string SupplierCNPJ { get; set; }
    }
}
