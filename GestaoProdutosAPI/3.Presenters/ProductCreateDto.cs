﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GestaoProdutosAPI.Presenters
{
    public class ProductCreateDto
    {
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }
        public DateTime ManufacturingDate { get; set; }
        public DateTime ExpirationDate { get; set; }
        public int SupplierId { get; set; }
        public string SupplierDescription { get; set; }
        public string SupplierCNPJ { get; set; }
    }
}
