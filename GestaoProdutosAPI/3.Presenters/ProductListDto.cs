﻿using GestaoProdutosAPI.Entities;
using System.Collections.Generic;

namespace GestaoProdutosAPI.Presenters  
{
    public class ProductListDto
    {
        public IEnumerable<ProductReadDto> Products { get; set; }
        public Paginator.Response Paginator { get; set; }
    }
}
