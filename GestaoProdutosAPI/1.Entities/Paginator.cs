﻿using DataAnnotationsExtensions;

namespace GestaoProdutosAPI.Entities
{
    public class Paginator
    {
        public class Request
        {
            [Min(1)]
            public int RowsPerPage { get; set; }
            [Min(1)]
            public int Page { get; set; }
        }
        public class Response
        {
            public int Page { get; set; }
            public int TotalRows { get; set; }
        }
    }
}
