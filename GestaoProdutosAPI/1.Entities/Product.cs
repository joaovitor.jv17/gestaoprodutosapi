﻿using System;
using System.ComponentModel.DataAnnotations;

namespace GestaoProdutosAPI.Entities
{
    public class Product
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        public bool Active { get; set; }
        public DateTime? ManufacturingDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int SupplierId { get; set; }
        public string SupplierDescription { get; set; }
        public string SupplierCNPJ { get; set; }

        public bool IsManufacturingDateInvalid()
        {
            return ManufacturingDate >= ExpirationDate;
        }
    }
}
