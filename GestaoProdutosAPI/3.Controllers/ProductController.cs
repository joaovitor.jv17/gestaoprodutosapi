﻿using AutoMapper;
using GestaoProdutosAPI.Presenters;
using GestaoProdutosAPI.Entities;
using GestaoProdutosAPI.UseCases;
using Microsoft.AspNetCore.Mvc;

namespace GestaoProdutosAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ProductService _productService;

        public ProductController(IMapper mapper, ProductService productService)
        {
            _mapper = mapper;
            _productService = productService;
        }

        [HttpGet]
        [Route("Get")]
        public IActionResult Get(int id)
        {
            Product product = _productService.Get(id);
            return Ok(_mapper.Map<ProductReadDto>(product));
        }

        [HttpPost]
        [Route("List")]
        public IActionResult List(ProductListRequest request)
        {
            var response = _productService.List(request);
            return Ok(_mapper.Map<ProductListDto>(response));
        }

        [HttpPost]
        [Route("Insert")]
        public IActionResult Insert(ProductCreateDto request)
        {
            var product = _mapper.Map<Product>(request);
            var newProduct = _productService.Insert(product);
            return Ok(_mapper.Map<ProductReadDto>(newProduct));
        }

        [HttpPut]
        [Route("Edit")]
        public IActionResult Edit(ProductEditDto request)
        {
            var product = _mapper.Map<Product>(request);
            _productService.Edit(product);
            return Ok("Product Edited Succesfully!");
        }

        [HttpDelete]
        [Route("Delete")]
        public IActionResult Delete(int id)
        {
            _productService.Delete(id);
            return Ok("Product Delleted Succesfully!");
        }
    }
}
