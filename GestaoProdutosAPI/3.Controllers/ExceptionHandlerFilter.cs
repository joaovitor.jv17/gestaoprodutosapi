﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Security.Authentication;
using System;

namespace GestaoProdutosAPI.Controllers
{
    public class ExceptionHandlerFilter : IExceptionFilter, IFilterMetadata
    {
        public void OnException(ExceptionContext context)
        {
            Exception exception = context.Exception;
            if (exception is ApplicationException)
            {
                context.Result = new BadRequestObjectResult(exception.Message);
                return;
            }

            context.Result = new BadRequestObjectResult("Something went wrong!!");
            return;
        }
    }
}
