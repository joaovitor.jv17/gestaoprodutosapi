﻿using GestaoProdutosAPI.Entities;
using System.Collections.Generic;

namespace GestaoProdutosAPI.UseCases
{
    public class ProductListResponse : Paginator
    {
        public IEnumerable<Product> Products { get; set; }
        public Paginator.Response Paginator { get; set; }
    }
}
