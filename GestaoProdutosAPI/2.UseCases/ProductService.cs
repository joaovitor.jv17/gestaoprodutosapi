﻿using GestaoProdutosAPI.Entities;
using System;


namespace GestaoProdutosAPI.UseCases
{
    public class ProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        public Product Get(int id)
        {
            Product product = _productRepository.Get(id);
            if (product == null) throw new ApplicationException("Product does not exist!");
            return product;
        }
        public ProductListResponse List(ProductListRequest request)
        {
            var products = _productRepository.List(request);
            var totalRows = _productRepository.ListCount(request);
            var response = new ProductListResponse()
            {
                Products = products,
                Paginator = new Paginator.Response { Page = request.Paginator.Page, TotalRows = totalRows }
            };
            return response;
        }
        public Product Insert(Product product)
        {
            CheckProduct(product);
            var newProduct = _productRepository.Insert(product);
            return newProduct;
        }
        public void Edit(Product product)
        {
            var currentProduct = _productRepository.Get(product.Id);
            if (currentProduct == null) throw new ApplicationException("Product does not exist!");
            CheckProduct(product);
            product.Active = currentProduct.Active;
            _productRepository.Edit(product);
        }
        public void Delete(int id)
        {
            var product = _productRepository.Get(id);
            if (product == null) throw new ApplicationException("Product does not exist!");
            product.Active = false;
            _productRepository.Delete(product);
        }
        private void CheckProduct(Product product)
        {
            if (string.IsNullOrEmpty(product.Description)) throw new ApplicationException("Description cannot be null or empty!");
            if (product.IsManufacturingDateInvalid()) throw new ApplicationException("Manufacturing date greater than expiration date!");
        }
    }
}
