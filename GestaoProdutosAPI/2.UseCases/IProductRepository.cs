﻿using GestaoProdutosAPI.Entities;
using System.Collections.Generic;

namespace GestaoProdutosAPI.UseCases
{
    public interface IProductRepository
    {
        void Delete(Product product);
        void Edit(Product product);
        Product Get(int id);
        Product Insert(Product product);
        IEnumerable<Product> List(ProductListRequest request);
        int ListCount(ProductListRequest request);
    }
}