﻿using GestaoProdutosAPI.Entities;
using System;

namespace GestaoProdutosAPI.UseCases
{
    public class ProductListRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? Active { get; set; }
        public DateTime? ManufacturingDateInitial { get; set; }
        public DateTime? ManufacturingDateEnd { get; set; }
        public DateTime? ExpirationDateInitial { get; set; }
        public DateTime? ExpirationDateEnd { get; set; }
        public string SupplierDescription { get; set; }
        public Paginator.Request Paginator { get; set; }
    }
}
